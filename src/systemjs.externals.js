module.exports.externals = [
  '@angular/animations/bundles/animations.umd.min.js',
  '@angular/animations/bundles/animations-browser.umd.min.js',
  '@angular/core/bundles/core.umd.min.js',
  '@angular/common/bundles/common.umd.min.js',
  '@angular/common/bundles/common-http.umd.min.js',
  '@angular/compiler/bundles/compiler.umd.min.js',
  '@angular/platform-browser/bundles/platform-browser.umd.min.js',
  '@angular/platform-browser/bundles/platform-browser-animations.umd.min.js',
  '@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.min.js',
  '@angular/http/bundles/http.umd.min.js',
  '@angular/router/bundles/router.umd.min.js',
  '@angular/router/bundles/router-upgrade.umd.min.js',
  '@angular/forms/bundles/forms.umd.min.js',
  '@angular/upgrade/bundles/upgrade.umd.min.js',
  '@angular/upgrade/bundles/upgrade-static.umd.min.js',
  '@apporchid/vulcanuxcore/vulcanuxcore.umd.min.js',
  '@apporchid/vulcanuxcontrols/vulcanuxcontrols.umd.min.js',
  '@apporchid/vulcanuxhighchart/vulcanuxhighchart.umd.min.js',
  '@apporchid/googlemapapi',
  '@apporchid/vulcanuxgooglemaps/vulcanuxgooglemaps.umd.min.js',
  '@apporchid/vulcanuxcontrols/themes/login/fonts',
  'core-js/client/shim.min.js',
  'zone.js/dist/zone.js',
  'systemjs/dist/system.src.js',
  'systemjs-plugin-css/css.js',
  'systemjs-plugin-text/text.js',
  'rxjs',
  'atmosphere.js/lib/atmosphere.js',
  'angular2-highcharts',
  'highcharts',
  '@xbs',
  'tslib/tslib.js',
  'angular-in-memory-web-api/bundles/in-memory-web-api.umd.min.js'
];


