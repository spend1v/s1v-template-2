var vuxCommonUtils = {
	toWxDateFormat : function(dateInMilliseconds) {
		if (dateInMilliseconds < 0) {
			return vuxCommonUtils.toWxTimeFormat(dateInMilliseconds);
		}
		return webix.i18n.dateFormatStr(new Date(dateInMilliseconds));
	},

	toWxLongDateFormat : function(dateInMilliseconds) {
		return webix.i18n.longDateFormatStr(new Date(dateInMilliseconds));
	},

	toWxFullDateFormat : function(dateInMilliseconds) {
		return webix.i18n.fullDateFormatStr(new Date(dateInMilliseconds));
	},

	toWxTimeFormat : function(dateInMilliseconds) {
		return webix.i18n.timeFormatStr(new Date(dateInMilliseconds));
	},
	booleanCompare : function(a, b) {
		if(b == "null") {
			return a === undefined || a == null;
		}
		return a !== undefined && a != null && a.toString() == b;
	},
	toWxDateYearFormat : function(year) {
		return webix.Date.dateToStr(new Date(year));
	},
	showTooltip: function(data) {
		return data.value;
	}
};