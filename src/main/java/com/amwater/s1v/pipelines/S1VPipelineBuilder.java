package com.amwater.s1v.pipelines;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.apporchid.cloudseer.common.pipeline.tasktype.DatasinkTaskType;
import com.apporchid.cloudseer.common.pipeline.tasktype.DatasourceTaskType;
import com.apporchid.cloudseer.datasink.db.RelationalDBDatasink;
import com.apporchid.cloudseer.datasink.db.RelationalDBDatasinkProperties;
import com.apporchid.cloudseer.datasource.xls.ExcelDatasource;
import com.apporchid.cloudseer.datasource.xls.ExcelDatasourceProperties;
import com.apporchid.foundation.mso.common.EDataUpdateType;
import com.apporchid.foundation.pipeline.tasktype.ITaskType;


@Component
public class S1VPipelineBuilder extends BasePipelineHelper {
	protected final Logger log = LoggerFactory.getLogger(S1VPipelineBuilder.class);
	
	private ITaskType<?,?>[] insertMasterDataFromExcelToDb() {
		
		DatasourceTaskType task1 = new DatasourceTaskType.Builder().name("Read data")
	               .datasourceType(ExcelDatasource.class).datasourcePropertiesType(ExcelDatasourceProperties.class)
	               .property(ExcelDatasourceProperties.EProperty.urlPath.name(), "excel/Cleansed-Spend-Master-Small-modified.xlsx")
	               .property(ExcelDatasourceProperties.EProperty.headerRowIndex.name(), 0).build();

	       DatasinkTaskType task2 = new DatasinkTaskType.Builder().name("Write data")
	               .datasinkType(RelationalDBDatasink.class).datasinkPropertiesType(RelationalDBDatasinkProperties.class)
	               .property(RelationalDBDatasinkProperties.EProperty.sqlDatasourceName.name(), "s1vSolutionDB")
	               .property(RelationalDBDatasinkProperties.EProperty.dataUpdateType.name(), EDataUpdateType.OVERWRITE)
	                .property(RelationalDBDatasinkProperties.EProperty.upsertKeyField.name(), "id")
	               .property(RelationalDBDatasinkProperties.EProperty.tableName.name(), "spendmaster").build();

	       
	       return new ITaskType<?,?>[] {task1, task2};
		
	}
	
	public void setupMasterData() throws Exception{
		
		// write all your pipelines to insert master data
		runNativePipeline("insertExcelToDb", insertMasterDataFromExcelToDb());
	}
}
