package com.amwater.s1v.pipelines;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amwater.common.IS1VConstants;
import com.apporchid.cloudseer.pipeline.domain.PipelineModel;
import com.apporchid.cloudseer.pipeline.service.PipelineCrudService;
import com.apporchid.cloudseer.pipeline.util.PipelineCreationHelper;
import com.apporchid.foundation.pipeline.IPipeline;
import com.apporchid.foundation.pipeline.IPipelineResult;
import com.apporchid.foundation.pipeline.tasktype.ITaskType;
import com.esri.arcgisruntime.internal.f.p;


public abstract class BasePipelineHelper {

	@Inject
	protected PipelineCrudService pipelineCrudService;
	protected final Logger log = LoggerFactory.getLogger(this.getClass());

	protected void savePipeline(IPipeline pipeline) throws Exception{
		pipeline.setDomainId(IS1VConstants.DEFAULT_DOMAIN_ID);
		pipeline.setSubDomainId(IS1VConstants.DEFAULT_SUB_DOMAIN_ID);
		
		PipelineModel pipelineModel = new PipelineModel();
		pipelineModel.setPipeline(pipeline);
		
		pipelineCrudService.create(pipelineModel);
	}
	
	protected IPipelineResult runNativePipeline(String pipelineName, ITaskType<?,?> [] tasks) throws Exception{
		
		IPipeline pipeline = PipelineCreationHelper.INSTNACE.createNativePipeline(pipelineName, tasks);
		
		savePipeline(pipeline);
		
		return run(pipeline);
	}

	// TODO:  write a method to run spark local/remote pipeline
	
	private IPipelineResult run(IPipeline pipeline) throws Exception {
		
		pipeline.build();
		
		IPipelineResult result = pipeline.run();
		
		log.debug("pipeline {} sucssessfully executed, results {} " , pipeline.getName(), result.getOutput());
		
		return result;
	}
}
