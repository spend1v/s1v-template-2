import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { enableProdMode } from '@angular/core';
import { MainModule } from "./solution/solution-launcher/main.module";
import { AppSettings } from './solution/solution-launcher/app.settings';

if (AppSettings.PRODUCTION) {
  enableProdMode()
}

platformBrowserDynamic().bootstrapModule(MainModule)
  .catch(err => console.log(err));
