package com.amwater.pipeline.test;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import com.amwater.s1v.pipelines.S1VPipelineBuilder;
import com.apporchid.cloudseer.CloudseerApplication;

@ActiveProfiles("test")
@SpringBootTest(classes = CloudseerApplication.class)
public class S1VPipelineTest extends AbstractTestNGSpringContextTests {

	protected final Logger log = LoggerFactory.getLogger(S1VPipelineTest.class);

	@Inject
	private S1VPipelineBuilder pipelineBuilder;
	
	@Test
	public void testInsertMasterDataToDb() throws Exception {
		pipelineBuilder.setupMasterData();
	}
}
