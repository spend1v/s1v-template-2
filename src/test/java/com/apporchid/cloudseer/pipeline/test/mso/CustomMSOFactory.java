package com.apporchid.cloudseer.pipeline.test.mso;

import java.util.Date;

import org.springframework.util.StringUtils;

import com.apporchid.common.cls.DynamicClassHelper;
import com.apporchid.core.common.UIDUtils;
import com.apporchid.core.common.cache.MSOCache;
import com.apporchid.core.mso.GroupMSO;
import com.apporchid.core.mso.config.DataSourcingSystemDefinition;
import com.apporchid.core.mso.config.datasource.RelationalDBColumnConfiguration;
import com.apporchid.core.mso.config.datasource.RelationalDBConfiguration;
import com.apporchid.core.mso.config.datasource.elasticsearch.Analyzer;
import com.apporchid.core.mso.config.datasource.elasticsearch.EElasticsearchDatatype;
import com.apporchid.core.mso.config.datasource.elasticsearch.ElasticsearchIndexSettings;
import com.apporchid.core.mso.config.datasource.elasticsearch.IAnalyzer;
import com.apporchid.core.mso.config.datasource.elasticsearch.ITokenFilter;
import com.apporchid.core.mso.config.datasource.elasticsearch.TokenFilter;
import com.apporchid.core.mso.feature.DataSourcingFeatures;
import com.apporchid.core.mso.util.MSODataObjectHelper;
import com.apporchid.foundation.common.ICommonConstants;
import com.apporchid.foundation.heuristics.IConfidenceMeasurement;
import com.apporchid.foundation.heuristics.IMetadataWithConfidence;
import com.apporchid.foundation.mso.IGroupMSO;
import com.apporchid.foundation.mso.IMSODataObject;
import com.apporchid.foundation.mso.common.EDataAccessType;
import com.apporchid.foundation.mso.common.EDataStorageType;
import com.apporchid.foundation.mso.config.IDataSourceFieldConfiguration;
import com.apporchid.foundation.mso.config.IDataSourcingSystemDefinition;
import com.apporchid.foundation.mso.feature.IDataSourcingFeatures;

public class CustomMSOFactory {
	public static final CustomMSOFactory INSTANCE = new CustomMSOFactory();

	public enum ECustomMSO {
		TestMSO("TestMSO",
				new String[] { "deviceId", "customerId", "time", "date", "status", "detectionType", "callType",
						"accidentId", "claimFlag", "vehicleType", "prefecture", "accidentType", "behaviorType",
						"placeType", "paymentTotalAmount" },
				new Class<?>[] { Long.class, String.class, Long.class, Date.class, Integer.class, Integer.class,
						Integer.class, Integer.class, String.class, String.class, String.class, String.class,
						String.class, String.class, Integer.class });
		String name;
		String[] fieldNames;
		Class<?>[] fieldTypes;

		private ECustomMSO(String name, String[] fieldNames, Class<?>[] fieldTypes) {
			this.name = name;
			this.fieldNames = fieldNames;
			this.fieldTypes = fieldTypes;
		}

		public String getName() {
			return name;
		};

		public String getDomainId() {
			return ICommonConstants.ROOT_DOMAIN;
		}

		public String getSubDomainId() {
			return ICommonConstants.ROOT_SUB_DOMAIN;
		}

		public String[] getFieldNames() {
			return fieldNames;
		}

		public Class<?>[] getFieldTypes() {
			return fieldTypes;
		}

		public String getClassName() {
			return getDomainId() + "." + getSubDomainId() + "." + getName();
		}

		public String getUID() {
			return UIDUtils.getUID(getDomainId(), getSubDomainId(), getName());
		}
	}

	private CustomMSOFactory() {
	}

	public IGroupMSO createMSO(ECustomMSO customMSO) {
		if (!DynamicClassHelper.INSTANCE.hasLoadedClass(customMSO.getClassName())) {
			GroupMSO mso = new GroupMSO(customMSO.getName());

			mso.setDomainId(customMSO.getDomainId());
			mso.setSubDomainId(customMSO.getSubDomainId());

			String[] fieldNames = customMSO.getFieldNames();
			Class<?>[] fieldTypes = customMSO.getFieldTypes();

			for (int i = 0; i < fieldNames.length; i++) {
				mso.addProperty(fieldNames[i], fieldTypes[i]);
			}

			IDataSourcingFeatures dataSourcingFeatures = null;
			switch (customMSO) {
			case TestMSO:
				dataSourcingFeatures = getTestMSODatasourcingFeatures(mso);
				break;
			default:
				break;
			}
			if (dataSourcingFeatures != null) {
				mso.setDataSourcingFeatures(dataSourcingFeatures);
			}
			return mso;
		}
		return (IGroupMSO) MSOCache.INSTANCE.get(customMSO.getUID());
	}

	private IDataSourcingFeatures getTestMSODatasourcingFeatures(IGroupMSO mso) {
		IDataSourcingFeatures datasourcingFeatures = new DataSourcingFeatures();

		// add database definition
		datasourcingFeatures.addDataSourcingSystemDefinition(createDBSystemDefinition(mso));

		// add elastic search definition
		datasourcingFeatures.addDataSourcingSystemDefinition(createElasticsearchIndexSettings(mso));
		return datasourcingFeatures;
	}

	protected IDataSourcingSystemDefinition createDBSystemDefinition(IGroupMSO mso) {
		IDataSourcingSystemDefinition systemDefinition = new DataSourcingSystemDefinition("TestDataDBSourcingSystem");

		systemDefinition.addDataAccessType(EDataAccessType.Source, true);
		systemDefinition.addDataAccessType(EDataAccessType.Sink, true);

		systemDefinition.addDataStorageType(EDataStorageType.Warm);

		RelationalDBConfiguration systemConfiguration = new RelationalDBConfiguration("postgresDS", "testdata");
		systemDefinition.setDataSourceSystemConfiguration(systemConfiguration);

		addDataSourceFieldConfigurations(systemDefinition, mso);

		return systemDefinition;

	}

	protected IDataSourcingSystemDefinition createElasticsearchIndexSettings(IGroupMSO mso) {

		IDataSourcingSystemDefinition systemDefinition = new DataSourcingSystemDefinition("ES-testdata");
		systemDefinition.addDataAccessType(EDataAccessType.Source);
		systemDefinition.addDataAccessType(EDataAccessType.Sink);
		systemDefinition.addDataStorageType(EDataStorageType.Hot);

		ElasticsearchIndexSettings indexSettings = new ElasticsearchIndexSettings("testdata", "csv");
		systemDefinition.setDataSourceSystemConfiguration(indexSettings);

		indexSettings.setNumberOfReplicas(1);
		indexSettings.setNumberOfShards(5);
		indexSettings.setRefreshIintervals("-1");

		IAnalyzer keywordAnalyzer = new Analyzer();
		keywordAnalyzer.setType("custom");
		keywordAnalyzer.setTokenizer("keyword");
		keywordAnalyzer.addTokenFilters("standard", "lowercase", "english_stemmer");
		indexSettings.getAnalysis().addAnalyzer("keyword_analyzer", keywordAnalyzer);

		ITokenFilter englishStemmer = new TokenFilter();
		englishStemmer.setType("stemmer");
		englishStemmer.add("name", "english");
		indexSettings.getAnalysis().addTokenFilter("english_stemmer", englishStemmer);
		indexSettings.getMappings().getProperties().addField("accidentId", EElasticsearchDatatype.Integer);
		indexSettings.getMappings().getProperties().addTextField("accidentType", "standard");
		indexSettings.getMappings().getProperties().addTextField("behaviorType", "standard");
		indexSettings.getMappings().getProperties().addTextField("callType", "standard");
		indexSettings.getMappings().getProperties().addTextField("claimFlag", "standard");
		indexSettings.getMappings().getProperties().addTextField("customerId", "standard");
		indexSettings.getMappings().getProperties().addDateField("date", null);
		indexSettings.getMappings().getProperties().addField("detectionType", EElasticsearchDatatype.Integer);
		indexSettings.getMappings().getProperties().addField("deviceId", EElasticsearchDatatype.Long);
		indexSettings.getMappings().getProperties().addField("paymentTotalAmount", EElasticsearchDatatype.Integer);
		indexSettings.getMappings().getProperties().addTextField("placeType", "standard");
		indexSettings.getMappings().getProperties().addTextField("prefecture", "standard");
		indexSettings.getMappings().getProperties().addField("status", EElasticsearchDatatype.Integer);
		indexSettings.getMappings().getProperties().addField("time", EElasticsearchDatatype.Long);
		indexSettings.getMappings().getProperties().addTextField("vehicleType", "standard");
		return systemDefinition;
	}

	protected IDataSourceFieldConfiguration getDBFieldConfiguration(String columnName, String dataType) {
		return new RelationalDBColumnConfiguration(columnName, dataType);
	}

	public IMSODataObject createMSODataObject(ECustomMSO customMSO) {
		return MSODataObjectHelper.getInstance(customMSO.getClassName());
	}

	private IDataSourcingSystemDefinition addDataSourceFieldConfigurations(IDataSourcingSystemDefinition sd,
			IGroupMSO mso) {
		Iterable<IMetadataWithConfidence<?, IConfidenceMeasurement<?>>> properties = mso.getProperties();

		properties.forEach(m -> {
			sd.addDataSourceFieldConfiguration(m.getMeasurableObject().getFieldName(),
					getDBFieldConfiguration(fromMsoToDbFieldName(m.getMeasurableObject().getFieldName()),
							fromMsoToDbFieldType(m.getMeasurableObject().getDataType())));
		});

		return sd;
	}

	private String fromMsoToDbFieldName(String msoFieldName) {

		if (msoFieldName != null) {
			String[] fieldNameTokens = msoFieldName.split("(?<!(^|[A-Z]))(?=[A-Z])|(?<!^)(?=[A-Z][a-z])");
			return StringUtils.arrayToDelimitedString(fieldNameTokens, "_").toLowerCase();
		}

		return msoFieldName;
	}

	private String fromMsoToDbFieldType(Class<?> dataType) {

		String returnDataType;

		switch (dataType.getName()) {

		case "java.lang.String":
			returnDataType = "VARCHAR";
			break;

		case "java.lang.Double":
		case "java.lang.Float":
		case "java.lang.Integer":
			returnDataType = "NUMBER";
			break;

		case "java.util.Date":
			returnDataType = "TIMESTAMP";
			break;

		default:
			returnDataType = "VARCHAR";
		}

		return returnDataType;
	}

}
