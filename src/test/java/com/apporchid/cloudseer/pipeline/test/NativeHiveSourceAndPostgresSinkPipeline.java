package com.apporchid.cloudseer.pipeline.test;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.apporchid.cloudseer.common.constants.ISparkConfigConstants;
import com.apporchid.cloudseer.common.pipeline.tasktype.DatasinkTaskType;
import com.apporchid.cloudseer.common.pipeline.tasktype.DatasourceTaskType;
import com.apporchid.cloudseer.datasink.db.RelationalDBDatasink;
import com.apporchid.cloudseer.datasink.db.RelationalDBDatasinkProperties;
import com.apporchid.cloudseer.datasink.text.TextDatasinkProperties;
import com.apporchid.cloudseer.datasource.db.HiveDatasource;
import com.apporchid.cloudseer.datasource.db.HiveDatasourceProperties;
import com.apporchid.cloudseer.pipeline.domain.PipelineModel;
import com.apporchid.cloudseer.pipeline.service.PipelineCrudService;
import com.apporchid.cloudseer.pipeline.util.PipelineCreationHelper;
import com.apporchid.core.common.cache.PipelineCache;
import com.apporchid.foundation.job.JobExecutionException;
import com.apporchid.foundation.mso.common.EDataUpdateType;
import com.apporchid.foundation.pipeline.IPipeline;

@Component
public class NativeHiveSourceAndPostgresSinkPipeline {
	protected final Logger log = LoggerFactory.getLogger(NativeHiveSourceAndPostgresSinkPipeline.class);
	@Inject
	private PipelineCrudService pipelineCrudService;
	
	private static final String query = "select * from ao_test.june28table";
	
	public void runPipeline(String pipelineName) throws Exception{
		IPipeline iPipeline = null;
		if (SystemUtils.IS_OS_WINDOWS)
			System.setProperty(ISparkConfigConstants.HADOOP_HOME_DIR, "/tmp/cloudseer/sparkResources/");
		if(StringUtils.isNotBlank(pipelineName)){
			try{
				iPipeline = PipelineCache.INSTANCE.get(pipelineName);
			}catch(Exception e){
				PipelineModel pipelineByName = pipelineCrudService.getPipelineByName(null, null, pipelineName);
				if(pipelineByName == null || pipelineByName.getPipeline() == null){
					
					iPipeline = createPipeline(pipelineName);
					com.apporchid.cloudseer.pipeline.domain.PipelineModel pipelineModel = new com.apporchid.cloudseer.pipeline.domain.PipelineModel();
					pipelineModel.setPipeline(iPipeline);
					pipelineCrudService.create(pipelineModel);
				}else {
					iPipeline = pipelineByName.getPipeline();
				}
			}
		}
		if(iPipeline != null){
			iPipeline.run();
		}else{
			throw new JobExecutionException("Pipeline ["+pipelineName+"] was not found in DB.");
		}
	}
	
	public IPipeline createPipeline(String name){
		
		DatasourceTaskType task1 = new DatasourceTaskType.Builder().name("Read hive data")
				.datasourceType(HiveDatasource.class).datasourcePropertiesType(HiveDatasourceProperties.class)
				.property(HiveDatasourceProperties.EProperty.sqlDatasourceName.name(), "hiveTestDB")
				.property(HiveDatasourceProperties.EProperty.sqlQuery.name(), query)
				.property(HiveDatasourceProperties.EProperty.isKerberized.name(), true)
				.property(HiveDatasourceProperties.EProperty.kerberosConfigName.name(), "HIVE_CLUSTER_264")
				.build();
		
		DatasinkTaskType task2 = new DatasinkTaskType.Builder().datasinkType(RelationalDBDatasink.class)
				.datasinkPropertiesType(RelationalDBDatasinkProperties.class)
				.property(RelationalDBDatasinkProperties.EProperty.sqlDatasourceName.name(), "testSolutionDB")
				.property(RelationalDBDatasinkProperties.EProperty.tableName.name(), "pg_native_hive_tbl"+System.currentTimeMillis())
				.property(RelationalDBDatasinkProperties.EProperty.dataUpdateType.name(), EDataUpdateType.OVERWRITE)
				.property(RelationalDBDatasinkProperties.EProperty.batchSize.name(), 10000)
				.build();
		
		try {
			return  PipelineCreationHelper.INSTNACE.createNativePipeline(name, task1,task2);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}

}
