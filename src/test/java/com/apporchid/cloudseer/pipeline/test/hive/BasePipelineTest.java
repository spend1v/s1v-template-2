package com.apporchid.cloudseer.pipeline.test.hive;

import static org.junit.Assert.assertTrue;
import static org.testng.Assert.assertEquals;

import java.io.File;
import java.io.IOException;

import javax.inject.Inject;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;

import com.apporchid.cloudseer.common.pipeline.PipelineOptions;
import com.apporchid.cloudseer.common.pipeline.tasktype.DatasinkTaskType;
import com.apporchid.cloudseer.common.pipeline.util.PipelineUtils;
import com.apporchid.cloudseer.datasink.log.LogDatasink;
import com.apporchid.cloudseer.datasink.log.LogDatasinkProperties;
import com.apporchid.cloudseer.datasink.output.OutputDatasink;
import com.apporchid.cloudseer.datasink.output.OutputDatasinkProperties;
import com.apporchid.cloudseer.datasource.pdf.PdfTextDatasourceProperties;
import com.apporchid.cloudseer.pipeline.domain.PipelineModel;
import com.apporchid.cloudseer.pipeline.service.PipelineCrudService;
import com.apporchid.cloudseer.pipeline.test.mso.CustomMSOFactory;
import com.apporchid.cloudseer.pipeline.test.mso.CustomMSOFactory.ECustomMSO;
import com.apporchid.cloudseer.pipeline.util.PipelineCreationHelper;
import com.apporchid.common.AOContext;
import com.apporchid.common.AOContextHolder;
import com.apporchid.core.common.cache.PipelineCache;
import com.apporchid.foundation.pipeline.EPipelineRunnerType;
import com.apporchid.foundation.pipeline.EPipelineStatus;
import com.apporchid.foundation.pipeline.IPipeline;
import com.apporchid.foundation.pipeline.IPipelineOptions;
import com.apporchid.foundation.pipeline.IPipelineResult;
import com.apporchid.foundation.pipeline.IPipelineRunnerProperties;
import com.apporchid.foundation.pipeline.tasktype.ITaskType;

import liquibase.logging.LogLevel;

public abstract class BasePipelineTest extends AbstractTestNGSpringContextTests {
	@Inject
	private PipelineCrudService pipelineCrudService;
	public static final String DEFAULT_DOMAIN_ID = "com.apporchid";
	public static final String DEFAULT_SUB_DOMAIN_ID = "samples";
	public static final String DEFAULT_SQL_DATASOURCE_NAME = "aoTestDB";

	static {
		AOContextHolder.setContext(new AOContext.Builder().withTenantId(1).withUserId("admin").build());

		for (ECustomMSO customMSO : ECustomMSO.values()) {
			CustomMSOFactory.INSTANCE.createMSO(customMSO);
		}
	}

	public BasePipelineTest() {
		super();
	}

	protected IPipelineResult createAndRunSparkPipeline(String pipelineName, EPipelineRunnerType runnerType,
			ITaskType<?, ?>... tasks) throws Exception {
		return createAndRunPipeline(pipelineName, -1, runnerType,
				PipelineCreationHelper.INSTNACE.getSparkSessionProperties(runnerType), tasks);
	}

	protected IPipelineResult createAndRunPipeline(String pipelineName, ITaskType<?, ?>... tasks) throws Exception {
		return createAndRunPipeline(pipelineName, -1, tasks);
	}

	protected String convertToJson(IPipeline pipeline) {
		return PipelineUtils.convertPipelineToJson(pipeline);
	}

	protected IPipeline convertFromJson(String json) {
		return PipelineUtils.convertJsonToPipeline(json);
	}

	protected IPipeline createPipeline(String pipelineName, ITaskType<?, ?>... tasks) throws Exception {
		return PipelineCreationHelper.INSTNACE.createPipeline(pipelineName, -1, tasks);
	}

	protected void savePipeline(IPipeline pipeline) throws Exception {
		pipeline.setDomainId(DEFAULT_DOMAIN_ID);
		pipeline.setSubDomainId(DEFAULT_SUB_DOMAIN_ID);

		PipelineModel pipelineModel = new PipelineModel();
		pipelineModel.setPipeline(pipeline);

		pipelineCrudService.create(pipelineModel);
	}

	protected IPipelineResult createAndRunPipeline(String pipelineName, int noOfRecordsToFetch,
			ITaskType<?, ?>... tasks) throws Exception {
		return createAndRunPipeline(pipelineName, noOfRecordsToFetch, EPipelineRunnerType.NATIVE, null, tasks);
	}

	protected IPipeline createPipeline(String pipelineName, int noOfRecordsToFetch, ITaskType<?, ?>... tasks)
			throws Exception {
		return PipelineCreationHelper.INSTNACE.createPipeline(pipelineName, noOfRecordsToFetch, tasks);
	}

	protected IPipeline createNativePipeline(String pipelineName, ITaskType<?, ?>... tasks) throws Exception {
		return PipelineCreationHelper.INSTNACE.createNativePipeline(pipelineName, tasks);
	}

	protected IPipeline createSparkLocalPipeline(String pipelineName, ITaskType<?, ?>... tasks) throws Exception {
		IPipeline pipeline = PipelineCreationHelper.INSTNACE.createSparkLocalPipeline(pipelineName, tasks);
		setPipelineOptions(pipeline.getPipelineOptions());

		//TODO:temporary fix
		PipelineCache.INSTANCE.put(pipeline);
		return pipeline;

	}

	protected IPipeline createSparkRemotePipeline(String pipelineName, ITaskType<?, ?>... tasks) throws Exception {
		return PipelineCreationHelper.INSTNACE.createSparkRemotePipeline(pipelineName, tasks);
	}

	protected IPipelineOptions getPipelineOptions(EPipelineRunnerType runnerType) {
		return new PipelineOptions().withRunnerType(runnerType, null);
	}

	protected void setPipelineOptions(IPipelineOptions pipelineOptions) {

	}

	protected IPipelineResult run(IPipeline pipeline) throws Exception {
		pipeline.build();
		IPipelineResult result = pipeline.run();
		System.out.println("finished execution... state: " + result.getPipelineStatus() + ", output type: "
				+ result.getOutputType() + ", output: " + result.getOutput());
		assertEquals(result.getPipelineStatus(), EPipelineStatus.COMPLETED);
		return result;
	}

	protected void runWithJsonSerialization(IPipeline pipeline) throws Exception {
		setPipelineOptions(pipeline.getPipelineOptions());
		String jsonStr = convertToJson(pipeline);
		System.out.println(jsonStr);
		pipeline = convertFromJson(jsonStr);
		if (jsonStr == null || pipeline == null) {
			assertTrue(false);
		}
		System.out.println(pipeline.getTopology());
		run(pipeline);
	}

	private IPipelineResult createAndRunPipeline(String pipelineName, int noOfRecordsToFetch,
			EPipelineRunnerType runnerType, IPipelineRunnerProperties runnerProperties, ITaskType<?, ?>... tasks)
			throws Exception {
		IPipeline pipeline = PipelineCreationHelper.INSTNACE.createPipeline(pipelineName, noOfRecordsToFetch,
				runnerType, runnerProperties, tasks);
		setPipelineOptions(pipeline.getPipelineOptions());
		pipeline.build();
		//System.out.println("Json->" + PipelineUtils.convertPipelineToJson(pipeline));

		IPipelineResult result = pipeline.run();
		System.out.println("finished execution... state: " + result.getPipelineStatus() + ", output type: "
				+ result.getOutputType());
		assertEquals(result.getPipelineStatus(), EPipelineStatus.COMPLETED);
		return result;
	}

	protected PdfTextDatasourceProperties pdfDatasourceProperties(String urlPath, int startPage, int endPage) {
		return pdfDatasourceProperties(urlPath, startPage, endPage, true);
	}

	protected PdfTextDatasourceProperties pdfDatasourceProperties(String urlPath, int startPage, int endPage,
			boolean covertToUrlPath) {
		PdfTextDatasourceProperties datasourceProperties = new PdfTextDatasourceProperties();
		datasourceProperties.setUrlPath(covertToUrlPath ? toUrlPath(urlPath) : urlPath);
		if (startPage > 0) {
			datasourceProperties.from(startPage);
		}
		if (endPage >= startPage) {
			datasourceProperties.till(endPage);
		}
		return datasourceProperties;
	}

	protected String toUrlPath(String urlPath) {
		try {
			if (this.applicationContext == null) {
				return new ClassPathResource(urlPath).getFile().getAbsolutePath();
			}
			Resource resource = this.applicationContext.getResource("classpath:" + urlPath);
			if (resource != null) {
				return resource.getFile().getAbsolutePath();
			}
		} catch (IOException e) {
			e.printStackTrace();
			urlPath = new File(urlPath).getPath();
		}
		return urlPath;
	}

	protected DatasinkTaskType getLogDataSinkTask() {
		DatasinkTaskType task5 = new DatasinkTaskType.Builder().name("Log data").datasinkType(LogDatasink.class)
				.datasinkPropertiesType(LogDatasinkProperties.class)
				.property(LogDatasinkProperties.EProperty.logLevel.name(), LogLevel.INFO).build();

		return task5;
	}

	protected DatasinkTaskType getOutputDataSinkTask() {
		DatasinkTaskType task5 = new DatasinkTaskType.Builder().name("Output data").datasinkType(OutputDatasink.class)
				.datasinkPropertiesType(OutputDatasinkProperties.class).build();

		return task5;
	}
	//
	// protected String toUrlPath(String urlPath) {
	// try {
	// urlPath = this.getClass().getResource(urlPath).toURI().getPath();
	// } catch (Throwable e) {
	// System.err.println(e.getMessage());
	// urlPath = new File(urlPath).getPath();
	// }
	// return urlPath;
	// }

	protected Object[][] toArgs(Object... args) {
		return new Object[][] { args };
	}
}
