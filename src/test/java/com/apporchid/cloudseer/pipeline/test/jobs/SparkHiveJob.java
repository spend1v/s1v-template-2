package com.apporchid.cloudseer.pipeline.test.jobs;

import java.util.Map;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apporchid.cloudseer.pipeline.test.SparkHiveSourceAndPostgresSinkPipeline;
import com.apporchid.foundation.job.JobExecutionException;
import com.apporchid.scheduler.AoServerCoreJob;

public class SparkHiveJob extends AoServerCoreJob {

	
	protected final Logger log = LoggerFactory.getLogger(SparkHiveJob.class);
	
	@Inject
	private SparkHiveSourceAndPostgresSinkPipeline pipeline;
	
	
	@Override 
	public void handleExecute(JobExecutionContext jec) throws Exception {
		try{
		log.info("job execution started jobname is " + jec.getJobDetail().getKey().getName());
		String pipelineName = getJobParameter(jec, "pipelineName");
		if(StringUtils.isNotBlank(pipelineName)){
			pipeline.runPipeline(pipelineName);
		}else{
			throw new JobExecutionException("Please check your job paramaeters.Pipeline name must be exist.");
		}
		log.info("job has executed successfully");
		}catch(Exception e){
			log.error(e.getMessage());
			throw e;
		}
		
		
	}
	
	
	protected String getJobParameter(JobExecutionContext jec,String name){
		Map<String,String> map = (Map) jec.getJobDetail().getJobDataMap().get("jobParameters");
		return map.get(name) == null ?  "" : map.get(name).toString() ;
	}

}
