package com.apporchid.cloudseer.pipeline.test;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.apporchid.cloudseer.common.constants.ISparkConfigConstants;
import com.apporchid.cloudseer.common.pipeline.tasktype.DatasinkTaskType;
import com.apporchid.cloudseer.common.pipeline.tasktype.DatasourceTaskType;
import com.apporchid.cloudseer.datasink.db.RelationalDBDatasink;
import com.apporchid.cloudseer.datasink.db.RelationalDBDatasinkProperties;
import com.apporchid.cloudseer.datasource.db.RelationalDBDatasource;
import com.apporchid.cloudseer.datasource.db.RelationalDBDatasourceProperties;
import com.apporchid.cloudseer.pipeline.domain.PipelineModel;
import com.apporchid.cloudseer.pipeline.service.PipelineCrudService;
import com.apporchid.cloudseer.pipeline.util.PipelineCreationHelper;
import com.apporchid.core.common.cache.PipelineCache;
import com.apporchid.foundation.job.JobExecutionException;
import com.apporchid.foundation.pipeline.IPipeline;

@Component
public class SparkHiveSourceAndPostgresSinkPipeline {
	protected final Logger log = LoggerFactory.getLogger(SparkHiveSourceAndPostgresSinkPipeline.class);
	@Inject
	private PipelineCrudService pipelineCrudService;
	
	private static final String query = "select * from maintenanceorderstatusts";
	
	public void runPipeline(String pipelineName) throws Exception{
		if (SystemUtils.IS_OS_WINDOWS)
			System.setProperty(ISparkConfigConstants.HADOOP_HOME_DIR, "/tmp/cloudseer/sparkResources/");
		IPipeline iPipeline = null;
		if(StringUtils.isNotBlank(pipelineName)){
			try{
				iPipeline = PipelineCache.INSTANCE.get(pipelineName);
			}catch(Exception e){
				PipelineModel pipelineByName = pipelineCrudService.getPipelineByName(null, null, pipelineName);
				if(pipelineByName == null || pipelineByName.getPipeline() == null){
					
					iPipeline = createPipeline(pipelineName);
					com.apporchid.cloudseer.pipeline.domain.PipelineModel pipelineModel = new com.apporchid.cloudseer.pipeline.domain.PipelineModel();
					pipelineModel.setPipeline(iPipeline);
					pipelineCrudService.create(pipelineModel);
				}else {
					iPipeline = pipelineByName.getPipeline();
				}
			}
		}
		if(iPipeline != null){
			iPipeline.run();
		}else{
			throw new JobExecutionException("Pipeline ["+pipelineName+"] was not found in DB.");
		}
	}
	
	public IPipeline createPipeline(String name){
		
		DatasourceTaskType hivedatasourceTask = new DatasourceTaskType.Builder().name("Read from HIVE")
				.datasourceType(RelationalDBDatasource.class)
				.datasourcePropertiesType(RelationalDBDatasourceProperties.class)
				.property(RelationalDBDatasourceProperties.EProperty.sqlDatasourceName.name(), "hiveTestDB")
				//.property(RelationalDBDatasourceProperties.EProperty.sqlQuery.name(), query)
				.property(RelationalDBDatasourceProperties.EProperty.tableName.name(), "ao_test.employ_1530175212832").build();

		DatasinkTaskType postgresDatasinkTask = new DatasinkTaskType.Builder().name("Write to Postgres")
				.datasinkType(RelationalDBDatasink.class).datasinkPropertiesType(RelationalDBDatasinkProperties.class)
				.property(RelationalDBDatasinkProperties.EProperty.sqlDatasourceName.name(), "testSolutionDB")
				.property(RelationalDBDatasinkProperties.EProperty.tableName.name(), "employFromhive_" + System.currentTimeMillis()).build();
		
		try {
			return  PipelineCreationHelper.INSTNACE.createSparkRemotePipeline(name, hivedatasourceTask, postgresDatasinkTask);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}

}
