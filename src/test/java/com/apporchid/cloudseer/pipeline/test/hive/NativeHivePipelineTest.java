package com.apporchid.cloudseer.pipeline.test.hive;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.apporchid.cloudseer.common.pipeline.tasktype.DatasinkTaskType;
import com.apporchid.cloudseer.common.pipeline.tasktype.DatasourceTaskType;
import com.apporchid.cloudseer.datasink.db.RelationalDBDatasink;
import com.apporchid.cloudseer.datasink.db.RelationalDBDatasinkProperties;
import com.apporchid.cloudseer.datasource.db.HiveDatasource;
import com.apporchid.cloudseer.datasource.db.HiveDatasourceProperties;
import com.apporchid.common.PlatformApplication;
import com.apporchid.foundation.mso.common.EDataUpdateType;
import com.apporchid.jpa.dbsetup.spring.config.AOPlatformPropertiesContextInitializerConfig;

@ActiveProfiles("test")
@SpringBootTest(classes = { PlatformApplication.class })
@ContextConfiguration(initializers = AOPlatformPropertiesContextInitializerConfig.class)
public class NativeHivePipelineTest extends BasePipelineTest {

	private static final String query = "select * from ao_test.june28table";
	
	@Test(dataProvider = "hiveSourceAndCsvSink")
	public void hiveSourceAndCsvSinkTest(DatasourceTaskType sourceTask, DatasinkTaskType sinkTask) throws Exception {
		createAndRunPipeline("Hive Source And CSV Sink", sourceTask, sinkTask);
	}

	@DataProvider(name = "hiveSourceAndCsvSink")
	protected Object[][] hiveSourceAndCsvSinkDatasource() {

		DatasourceTaskType task1 = new DatasourceTaskType.Builder().name("Read hive data")
				.datasourceType(HiveDatasource.class).datasourcePropertiesType(HiveDatasourceProperties.class)
				.property(HiveDatasourceProperties.EProperty.sqlDatasourceName.name(), "hiveTestDB")
				.property(HiveDatasourceProperties.EProperty.sqlQuery.name(), query)
				.property(HiveDatasourceProperties.EProperty.isKerberized.name(), true)
				.property(HiveDatasourceProperties.EProperty.kerberosConfigName.name(), "HIVE_CLUSTER_264")
				.build();
		
		DatasinkTaskType task2 = new DatasinkTaskType.Builder().datasinkType(RelationalDBDatasink.class)
				.datasinkPropertiesType(RelationalDBDatasinkProperties.class)
				.property(RelationalDBDatasinkProperties.EProperty.sqlDatasourceName.name(), "postgresDB")
				.property(RelationalDBDatasinkProperties.EProperty.tableName.name(), "pg_native_hive_tbl"+System.currentTimeMillis())
				.property(RelationalDBDatasinkProperties.EProperty.dataUpdateType.name(), EDataUpdateType.OVERWRITE)
				.property(RelationalDBDatasinkProperties.EProperty.batchSize.name(), 10000)
				.build();

		return new Object[][] { { task1, task2 } };
	}

}