import { NgModule, Injector } from '@angular/core';
import { VulcanuxCoreModule, WebixComponentsRegistry } from '@apporchid/vulcanuxcore';
import { VulcanuxControlsModule } from '@apporchid/vulcanuxcontrols';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SampleComponent } from './components/sample/sample.component';


//import { SystemTypeDataViewDetails } from "./components/entity-search-results/system-dataview-details";
//import { SystemTypeRelatedDataview } from "./components/shared/systemtype-realted-details";

const entryComponents = [
    SampleComponent
];
@NgModule({
    declarations: [...entryComponents],
    imports: [
        VulcanuxCoreModule,
        VulcanuxControlsModule,
        HttpModule,
        CommonModule,
        FormsModule,
        HttpClientModule,
    ],
    entryComponents: [...entryComponents],
})
export class SolutionModule {
    constructor(private injector: Injector) {
        WebixComponentsRegistry(entryComponents, injector)
    }
}