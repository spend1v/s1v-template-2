"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var vulcanuxcontrols_1 = require("@apporchid/vulcanuxcontrols");
var appstore_constants_1 = require("../../constants/appstore-constants");
var router_1 = require("@angular/router");
var app_settings_1 = require("../../app.settings");
var vulcanuxcore_1 = require("@apporchid/vulcanuxcore");
var SettingsInfoThumbnail = (function (_super) {
    __extends(SettingsInfoThumbnail, _super);
    function SettingsInfoThumbnail(router, route, rootelement, changes, zone) {
        var _this = _super.call(this, '', '') || this;
        _this.router = router;
        _this.route = route;
        _this.rootelement = rootelement;
        _this.changes = changes;
        _this.zone = zone;
        _this.name = 'settingsinfo-thumbnail';
        _this.checkValue = 'off';
        return _this;
    }
    Object.defineProperty(SettingsInfoThumbnail.prototype, "data", {
        get: function () {
            return this._data;
        },
        set: function (arg) {
            this.initializeSettingsInfoThumbnail(arg);
            this._data = arg;
        },
        enumerable: true,
        configurable: true
    });
    SettingsInfoThumbnail.prototype.initializeSettingsInfoThumbnail = function (data) {
        var _this = this;
        //data.imageUrl = data.solutionName === 'appstore' ? 'assets/images/appstore-icon.png' : "assets/images/thumbnails/chat.png";
        this.checkValue = 'off';
        this.checkValue = data.default === true ? 'on' : 'off';
        this.thumbnailConfig = {
            view: 'layout',
            paddingY: 20,
            css: 'settings_solution_item',
            minHeight: 237,
            // minWidth: 600,
            // maxWidth: 1700,
            cols: [{
                    view: 'panel',
                    width: 128,
                    height: 128,
                    body: {
                        view: 'label',
                        css: 'image_holder',
                        label: '<img src=' + this.getIconUrl(data) + '></img>'
                    }
                }, { width: 20 }, {
                    view: 'layout',
                    rows: [
                        {
                            view: 'panel',
                            height: 30,
                            body: {
                                view: 'label',
                                label: data.displayName,
                                height: 30,
                                css: 'vux_h5'
                            }
                        },
                        { type: 'space', height: 2 }, {
                            view: 'panel',
                            minHeight: 32,
                            css: 'settings_description',
                            body: {
                                view: 'vux-description',
                                data: data.description,
                                css: 'vux_p',
                                height: 32,
                                config: {
                                    charLimit: 250,
                                    readmore: true
                                }
                            }
                        },
                        {
                            view: 'panel',
                            body: {
                                view: 'label',
                                label: '<span class="vuxicon-business-male-user-solid"></span>  ' + appstore_constants_1.AppstoreConstants.settings.CREATED_BY + "<b>" + this.getValue(data, 'createdBy') + "</b>",
                                css: 'solutions_created_on'
                            }
                        },
                        //     {
                        //     view: 'layout',
                        //     cols: []
                        // }, 
                        // {
                        //     view: 'panel',
                        //     body: {
                        //         view: 'label',
                        //         label: AppstoreConstants.settings.CREATED_ON + "<b>" + data.createdDate + "</b>",
                        //         css: 'solutions_created_on'
                        //     }
                        // }, 
                        { type: 'space', height: 10 }, {
                            view: 'layout',
                            css: 'soltuion_item_footer',
                            height: 30,
                            // hidden: data.allowEdit ? false: true,
                            cols: [
                                {
                                    view: 'panel',
                                    width: 228,
                                    css: 'default_radio',
                                    body: {
                                        view: "radio", name: "gr1x", css: data.isDefault === true ? 'webix_radio_option selected' : 'webix_radio_option',
                                        id: "solution-page_radio_btn", value: data.isDefault === true ? 1 : '0',
                                        checkValue: data.isDefault === true ? 'on' : 'off',
                                        width: 228,
                                        options: [
                                            { id: 1, value: data.isDefault ? appstore_constants_1.AppstoreConstants.settings.DEFAULT_SOLUTION : appstore_constants_1.AppstoreConstants.settings.MAKE_DEFAULT_SOLUTION }
                                        ],
                                        defaultSolutionName: "",
                                    }
                                },
                                { type: 'space', height: 30, width: 20 },
                                { type: 'space', height: 30, width: 1, css: 'vertical_line', hidden: data.allowEdit ? false : true, },
                                { type: 'space', height: 30, width: 20 },
                                {
                                    view: 'button',
                                    height: 31,
                                    id: data.displayName,
                                    width: 86,
                                    // paddingY: 3,
                                    //label: 'Edit',
                                    type: 'iconButton',
                                    icon: 'vuxicon-edit-page-light',
                                    label: 'Edit',
                                    hidden: data.allowEdit ? false : true,
                                    //label:"<span class='webix_icon fa-envelope'>EDIT</span>",
                                    //label: '<span class="fa fa-pencil-square-o" aria-hidden="true"></span>'+"EDIT",
                                    click: function () { return _this.onEdit(data.solutionId); }
                                },
                                // {
                                //     view: 'button',
                                //     height: 31,
                                //     id: 'addApps',
                                //     width:124,
                                //    // paddingY: 3,
                                //     //label: 'Edit',
                                //     type:'iconButton',
                                //     icon: 'vuxicon-add-round-light',
                                //     label:'ADD APPS',
                                //     hidden: data.allowEdit ? false: true,
                                //     //label:"<span class='webix_icon fa-envelope'>EDIT</span>",
                                //     //label: '<span class="fa fa-pencil-square-o" aria-hidden="true"></span>'+"EDIT",
                                //    // click: () => this.onEdit(data.solutionId)
                                // },                         
                                // {
                                //     view: 'panel',
                                //     autowidth: true,
                                //     css: 'remove-button',                       
                                //     hidden: data.allowEdit ? false: true,
                                //     body: 
                                // },
                                {
                                    view: 'button',
                                    width: 114,
                                    css: 'remove-button',
                                    icon: 'vuxicon-trash-regular',
                                    hidden: data.allowEdit ? false : true,
                                    type: 'iconButton',
                                    label: appstore_constants_1.AppstoreConstants.settings.REMOVE,
                                    click: function () { return _this.onRemove(data, data.solutionId); }
                                },
                                { type: 'space', height: 30, width: 20 },
                                { type: 'space', height: 30, width: 1, css: 'vertical_line', hidden: data.allowEdit ? false : true, },
                                { type: 'space', height: 30, width: 20 },
                                { css: 'last_modified_container', cols: [{
                                            view: 'label',
                                            label: appstore_constants_1.AppstoreConstants.settings.LASTMODIFIED_ON + '<span class="last_modified_time">' + this.getValue(data, 'lastModifiedDate') + '</span>',
                                            css: 'last_modified_heading right'
                                        },
                                        { type: 'space', height: 30, width: 15 },
                                        { type: 'space', height: 30, width: 1, css: 'vertical_line' },
                                        { type: 'space', height: 30, width: 15 },
                                        {
                                            view: 'label',
                                            label: appstore_constants_1.AppstoreConstants.settings.CREATED_ON + '<span class="last_modified_time">' + data.createdDate + '</span>',
                                            css: 'last_modified_heading'
                                        }] }
                            ]
                        }
                    ]
                }]
        };
    };
    SettingsInfoThumbnail.prototype.makeDefaultSolution = function (solutionId) {
        // let path = AppSettings.SETTINGS_PAGE_SOLUTION_DEFAULT + '/' + solutionId;
        //  this.baseService.get(path).subscribe((res) => {
        //    if (res) {
        //       // this.emit(new CustomVUXEvent('OnSolutionRemove', this.name, res, this));
        //     // this.dataList = this.changeDefaultSolution(solutionId);
        //    }
        //  });
    };
    SettingsInfoThumbnail.prototype.onEdit = function (id) {
        var _this = this;
        if (this.getElementRef('SettingsWindow')) {
            this.getElementRef('SettingsWindow').close();
        }
        var data = {
            solutionId: id
        };
        this.zone.run(function () {
            _this.router.navigate(['./appstore/solutioncomposer', data]);
        });
        this.changes.detectChanges();
    };
    SettingsInfoThumbnail.prototype.onRemove = function (record, data) {
        var dataviewObj = webix.$$('settings-dataview-id');
        var dvrecord = dataviewObj.getItem(record.id);
        // alert('remove called--'+JSON.stringify(record));
        //alert('dviecrecord called--'+JSON.stringify(dvrecord));
        //alert('dviecrecord length--'+dataviewObj.config.data.length);
        this.showModel(record, data, dataviewObj);
    };
    SettingsInfoThumbnail.prototype.showModel = function (record, data, dataviewObj) {
        var _this = this;
        var appsContent = '';
        var modelTitle = '<strong> Whether you want to delete the solution </strong>';
        modelTitle = modelTitle + ':' + record.displayName;
        //webix.confirm({
        webix.modalbox({
            title: "Delete Record Confirmation",
            id: "confirm-model",
            ok: "YES",
            cancel: "CANCEL",
            type: "confirm-error",
            height: 160,
            minHeight: 160,
            width: 583,
            text: modelTitle,
            callback: function (result) {
                if (result) {
                    if (_this.getElementRef('SettingsWindow')) {
                        _this.getElementRef('SettingsWindow').close();
                    }
                    _this.removeSolution(record, data, dataviewObj);
                }
                else {
                    //this.refreshTable();
                    //this.cancelPopup(solutionId, data);
                }
            }
        });
    };
    SettingsInfoThumbnail.prototype.removeSolution = function (record, data, dataviewObj) {
        var _this = this;
        dataviewObj.remove(record.id);
        dataviewObj.refresh();
        if (this.getElementRef('settingsWindow')) {
            this.getElementRef('settingsWindow').hide();
        }
        var response;
        var path = app_settings_1.AppSettings.SETTINGS_PAGE_SOLUTION_REMOVE + '/' + record.solutionId;
        this.baseService.get(path).subscribe(function (res) {
            if (res) {
                _this.emit(new vulcanuxcore_1.CustomVUXEvent('OnSolutionRemove', 'settings', record.solutionId, _this));
                dataviewObj.refresh();
            }
        });
    };
    SettingsInfoThumbnail.prototype.getValue = function (data, key) {
        return (data && data[key]) ? data[key] : '--';
    };
    SettingsInfoThumbnail.prototype.getIconUrl = function (obj) {
        var icon = '';
        if (obj.imageUrl) {
            if (obj.imageUrl && obj.imageUrl.indexOf('assets') > -1) {
                icon = obj.imageUrl;
            }
            else {
                icon = vulcanuxcore_1.AppSettings.BASE_URL_MAIN + 'vux-resources/solution/' + obj.solutionName + '/' + obj.imageUrl;
            }
        }
        return icon;
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], SettingsInfoThumbnail.prototype, "data", null);
    SettingsInfoThumbnail = __decorate([
        core_1.Component({
            selector: 'settingsinfo-thumbnail',
            templateUrl: './settingsinfo-thumbnail.component.html',
        }),
        __metadata("design:paramtypes", [router_1.Router, router_1.ActivatedRoute, core_1.ElementRef,
            core_1.ChangeDetectorRef,
            core_1.NgZone])
    ], SettingsInfoThumbnail);
    return SettingsInfoThumbnail;
}(vulcanuxcontrols_1.VUXComponent));
exports.SettingsInfoThumbnail = SettingsInfoThumbnail;
