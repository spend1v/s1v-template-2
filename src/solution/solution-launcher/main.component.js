"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var vulcanuxcore_1 = require("@apporchid/vulcanuxcore");
var MainComponent = (function () {
    function MainComponent(changeDetectorRef, router, zone) {
        var _this = this;
        this.changeDetectorRef = changeDetectorRef;
        this.router = router;
        this.zone = zone;
        // <vulcanux-textarea [config]="config" [data]="data"></vulcanux-textarea>
        // <button (click)="changeConfig()" > ChanageConfig</button>
        // <button (click)="changeData()" > ChanageConfig</button>
        // <button (click)="updateConfig()" > UpdateConfig</button>
        this.config = {
            labelWidth: 200,
            width: 500,
            height: 50,
            label: 'onChangesTesting'
        };
        this.data = 'data initial';
        this.changeConfig = function () {
            _this.config = {
                labelWidth: 200,
                width: 800,
                height: 150,
                label: 'labelChanged'
            };
        };
        this.changeData = function () {
            _this.data = "chnaged";
        };
        this.updateConfig = function () {
            _this.config = Object.assign({}, _this.config);
            _this.config.label = 'labelUpdated';
            _this.config['value'] = 'hi';
        };
        vulcanuxcore_1.WebixUtil.hideProgress();
        // try {
        //   SocketService.openSocket();
        //   SocketService.dataStream.subscribe((res: any) => {
        //     if(res && res.notificationType === 'GLOBAL') {
        //       webix.message('global notification' + res.message);
        //     }
        //   });
        // } catch (error) {
        //   console.error('error in socket connection', error)
        // }
        this.authErrorSubscription = vulcanuxcore_1.EventManager.getInstance().subscribe('401refresh_token_expired', function () {
            localStorage.setItem('sessionStatus', 'expired');
            console.log('Token expired');
            _this.zone.run(function () {
                try {
                    _this.router.navigate(['/login']);
                }
                catch (error) {
                    console.error('login route error', error);
                }
            });
        });
        this.router.events.subscribe(function (event) {
            if (event instanceof router_1.RouteConfigLoadStart) {
                vulcanuxcore_1.WebixUtil.showProgress();
            }
            else if (event instanceof router_1.RouteConfigLoadEnd) {
                vulcanuxcore_1.WebixUtil.hideProgress();
            }
        });
    }
    MainComponent.prototype.ngOnDestroy = function () {
        console.log('destroy got called');
        this.authErrorSubscription.unsubscribe();
        vulcanuxcore_1.UsageAnalytics.getInstance().sendLogoutEvent(false, true);
    };
    MainComponent = __decorate([
        core_1.Component({
            selector: 'app-main',
            templateUrl: './main.component.html'
        }),
        __metadata("design:paramtypes", [core_1.ChangeDetectorRef, router_1.Router, core_1.NgZone])
    ], MainComponent);
    return MainComponent;
}());
exports.MainComponent = MainComponent;
