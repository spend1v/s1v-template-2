"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var vulcanuxcore_1 = require("@apporchid/vulcanuxcore");
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var Observable_1 = require("rxjs/Observable");
var cart_service_1 = require("../../services/cart-service");
var app_settings_1 = require("../../../../app.settings");
var app_util_1 = require("../../../../utils/app-util");
var appstore_constants_1 = require("../../../../constants/appstore-constants");
var vulcanuxcore_2 = require("@apporchid/vulcanuxcore");
var breacrumb_config_util_1 = require("./config-utils/breacrumb.config.util");
var dom_util_1 = require("../../../../utils/dom-util");
var AppDetailsComponent = (function (_super) {
    __extends(AppDetailsComponent, _super);
    function AppDetailsComponent(router, route, zone, cartService, appUtil, changes, relayService) {
        var _this = _super.call(this) || this;
        _this.router = router;
        _this.route = route;
        _this.zone = zone;
        _this.cartService = cartService;
        _this.appUtil = appUtil;
        _this.changes = changes;
        _this.relayService = relayService;
        return _this;
    }
    Object.defineProperty(AppDetailsComponent.prototype, "data", {
        get: function () {
            return this._data;
        },
        set: function (arg) {
            this._data = arg;
            this.initializeAppDetailApp(arg);
        },
        enumerable: true,
        configurable: true
    });
    AppDetailsComponent.prototype.onCreate = function () {
        var _this = this;
        this._onToolbarClick = this.subscribe('appstore-toolbaronSelect', function (event) {
            _this.navigateToHome(event.data);
        });
        this.cartServiceChange = this.subscribe('CartServicechangeincart', function (event) {
            var allreadyInVCart = false;
            event.data.map(function (item) {
                if (item.id === _this._data.uid) {
                    allreadyInVCart = true;
                }
            });
            var addbtn = _this.getElementRef('detail-add-btn');
            addbtn.define('disabled', allreadyInVCart);
            addbtn.refresh();
        });
    };
    AppDetailsComponent.prototype.initializeAppDetailApp = function (data) {
        var _this = this;
        var alreadyInCart = this.cartService.isExistInCart(this._data.uid);
        var rateConfig = this.setRatingDefinition(data);
        this.categoryName = this.relayService.getAppDetailData()['category'];
        this.categoryId = this.relayService.getAppDetailData()['categoryId'];
        dom_util_1.DOMUtil.activateToolBarbutton(appstore_constants_1.AppstoreConstants.CATEGORY_UNIQ_PREFIX + this.categoryId);
        var breadcrumbConfig = new breacrumb_config_util_1.BreadCrumbConfig().getConfig(this.categoryId, this.categoryName, data.displayName, this.navigateToHome.bind(this), this.navigateToHome.bind(this));
        var rightColumnConfig = {
            minWidth: 200,
            maxWidth: 600,
            width: 400,
            css: 'details-right-column',
            rows: [
                {
                    view: 'panel',
                    body: {
                        view: 'label',
                        label: '<img src=assets/images/thumbnails/' + data.thumbnail + '>',
                        align: 'center',
                        width: 200,
                        height: 200
                    },
                    paddingX: 95,
                    paddingY: 70,
                    css: {
                        'background-color': data.thumbnailBgcolor ? data.thumbnailBgcolor : '#024959',
                        'border-radius': '4px'
                    }
                }, { height: 26 }, {
                    view: 'panel',
                    align: 'center',
                    paddingY: 7,
                    paddingX: 112,
                    height: 48,
                    body: {
                        view: 'app-rating',
                        config: {
                            disabled: true
                        },
                        data: data['overallRating'] ? data['overallRating'] : 0,
                        css: 'review_rating img_bottom_rating',
                        width: 171,
                    }
                }, { height: 30 },
                {
                    view: 'panel',
                    autoheight: true,
                    body: {
                        view: 'label', id: 'details-title', label: data.displayName ? data.displayName : data.name, align: 'center', css: 'appdetails_main_heading',
                    }
                }, { height: 30 },
                {
                    view: 'panel',
                    paddingY: 40,
                    css: 'appdetails_add_btn',
                    body: {
                        view: 'button', id: 'detail-add-btn', disabled: alreadyInCart,
                        css: 'appdetails_add_button',
                        label: appstore_constants_1.AppstoreConstants.appdetails.ADD_BTN, height: 57, width: 108, align: 'center',
                        click: function () { _this.addAppToCart(); }
                    }
                },
                {
                    view: 'panel',
                    paddingY: 26,
                    css: 'details_attributes',
                    body: {
                        height: 30,
                        cols: [{}, {
                                cols: [{
                                        view: 'vux-icon',
                                        icon: 'eye'
                                    }, {
                                        view: 'label', label: data.totalViews ? data.totalViews : 0, css: 'details-views-count'
                                    }]
                            }, {
                                cols: [{
                                        view: 'icon',
                                        icon: 'comment-o'
                                    }, {
                                        view: 'label', label: data.totalComments, css: 'details-comment-count'
                                    }]
                            }, {}]
                    }
                }, { height: 22 },
                {
                    view: 'panel',
                    paddingY: 4,
                    body: {
                        view: 'label', label: appstore_constants_1.AppstoreConstants.appdetails.PUBLISHED, align: 'center', height: 25, css: 'appdetails_label_heading'
                    },
                },
                {
                    view: 'panel',
                    paddingY: 4,
                    body: {
                        view: 'label', label: data.createdDate,
                        align: 'center', height: 25, css: 'appdetails_data_heading'
                    }
                }, { view: 'spacer', height: 17 },
                {
                    view: 'panel',
                    paddingY: 3,
                    body: {
                        view: 'label', label: appstore_constants_1.AppstoreConstants.appdetails.PUBLISHED_BY, align: 'center', height: 25, css: 'appdetails_label_heading'
                    }
                },
                {
                    view: 'panel',
                    paddingY: 3,
                    body: {
                        view: 'label', label: '<img src="assets/images/ao-logo-details-page.png" alt=""/>', height: 50, align: 'center', css: 'details-publishedby-val'
                    }
                }, { height: 15 },
                {
                    view: 'panel',
                    body: {
                        view: 'label', label: appstore_constants_1.AppstoreConstants.appdetails.CURRENT_VERSION, align: 'center', height: 25, css: 'appdetails_label_heading'
                    }
                },
                {
                    view: 'panel',
                    body: {
                        view: 'label', label: data.version, align: 'center', height: 25, css: 'appdetails_data_heading'
                    }
                }, {
                    height: 20,
                    hidden: !data.lastModified,
                },
                {
                    view: 'panel',
                    paddingY: 3,
                    body: {
                        view: 'label', label: appstore_constants_1.AppstoreConstants.appdetails.UPDATED_ON,
                        hidden: !data.lastModified,
                        align: 'center', height: 25, css: 'appdetails_label_heading'
                    }
                },
                {
                    view: 'panel',
                    paddingY: 3,
                    body: {
                        view: 'label', label: data.lastModified,
                        hidden: !data.lastModified,
                        align: 'center', height: 25, css: 'appdetails_data_heading'
                    }
                }, { height: 14, hidden: !data.updateInfo },
                {
                    view: 'panel',
                    body: {
                        view: 'label', label: appstore_constants_1.AppstoreConstants.appdetails.APP_UPDATES,
                        hidden: !data.updateInfo,
                        align: 'center', height: 25, css: 'appdetails_label_heading'
                    }
                },
                {
                    view: 'panel',
                    body: {
                        view: 'label', label: appstore_constants_1.AppstoreConstants.appdetails.WHATS_NEW,
                        hidden: !data.updateInfo,
                        align: 'center', height: 25, css: 'appdetails_link'
                    }
                }, {
                    height: 28,
                    css: 'details-rightcolumn-end'
                }
            ]
        };
        var leftColumnConfig = {
            minWidth: 300,
            maxWidth: 2000,
            autowidth: true,
            paddingX: 2,
            css: 'details-left-col-css',
            rows: [
                {
                    view: 'panel', body: {
                        view: 'label', label: data.displayName ? data.displayName : data.name, css: 'appdetails_main_title'
                    }
                },
                {
                    view: 'panel',
                    paddingY: 16,
                    css: 'appdetails_description',
                    body: {
                        view: 'vux-description',
                        data: data.description,
                        css: 'vux_p',
                        config: {
                            charLimit: 250,
                            readmore: true
                        }
                    }
                },
                {
                    view: 'panel',
                    css: 'appdetails_multiview',
                    paddingY: 20, body: {
                        view: 'appstore-details-multiview',
                        data: data
                    }
                },
                {
                    id: 'detail-overalration',
                    view: 'appstore-overallrating',
                    config: {
                        title: 'Customer Ratings'
                    },
                    data: rateConfig,
                    css: 'details-customer-ratings',
                },
                {
                    view: 'appstore-related-app',
                    data: data.relatedApps,
                    css: 'appstore-scroll appdetail-related-dataview'
                },
                {
                    view: 'appstore-reviews',
                    data: data,
                    appId: data.applicationId,
                    css: 'details-reviews-container',
                }
            ]
        };
        setTimeout(function () {
            _this.detailsConfig = {
                scrollY: true,
                paddingY: 6,
                paddingX: 30,
                css: 'appdetails_container',
                autoheight: true,
                rows: [
                    breadcrumbConfig,
                    {
                        css: 'details-content-column',
                        cols: [
                            leftColumnConfig,
                            { width: 120 },
                            rightColumnConfig
                        ]
                    }
                ]
            };
            _this.changes.detectChanges();
            setTimeout(function () {
                dom_util_1.DOMUtil.showBanner(false);
                dom_util_1.DOMUtil.showCategories(false);
            }, 500);
        }, 100);
    };
    AppDetailsComponent.prototype.onInitialize = function () {
        this.appUtil.showHideBanner(false);
    };
    AppDetailsComponent.prototype.navigateToHome = function (categoryId) {
        var _this = this;
        var data = {
            selectedCategory: categoryId
        };
        this.zone.run(function () {
            _this.router.navigate(['./', data]);
            dom_util_1.DOMUtil.showBanner(true);
            dom_util_1.DOMUtil.showCategories(true);
        });
    };
    AppDetailsComponent.prototype.onDestroy = function () {
        this._onToolbarClick.unsubscribe();
        this.cartServiceChange.unsubscribe();
        this.appUtil.showHideBanner(true);
    };
    AppDetailsComponent.prototype.addAppToCart = function () {
        var _this = this;
        var data = { 'id': this._data.uid, 'name': this._data.name, 'thumbnails': this._data.thumbnail, 'displayName': this._data.displayName };
        this.baseService.get(app_settings_1.AppSettings.getAddAppToCartURL(data.id)).catch(function (error, r) {
            if (error) {
                // TODO: handle error
                return Observable_1.Observable.of({ error: error });
            }
            else {
                return r;
            }
        }).subscribe(function (res) {
            if (res.error) {
                // do nothing
            }
            else {
                _this.cartService.addApp(data);
                var addbtn = _this.getElementRef('detail-add-btn');
                addbtn.define('disabled', true);
                addbtn.refresh();
            }
        });
    };
    AppDetailsComponent.prototype.setRatingDefinition = function (data) {
        var ratingConfig = {
            userRating: data.appRatings.userRating,
            overallRating: data.appRatings.userRating,
            totalUsers: data.appRatingstotalUsers,
            ratingDivisions: [{
                    rating: 1,
                    users: 0
                }, {
                    rating: 2,
                    users: 0
                }, {
                    rating: 3,
                    users: 0
                }, {
                    rating: 4,
                    users: 0
                }, {
                    rating: 5,
                    users: 0
                }]
        };
        if (data.appRatings['ratingDivisions']) {
            data.appRatings['ratingDivisions'].map(function (record) {
                ratingConfig['ratingDivisions'][record.rating - 1] = record;
            });
        }
        return ratingConfig;
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], AppDetailsComponent.prototype, "data", null);
    AppDetailsComponent = __decorate([
        core_1.Component({
            selector: 'appstore-app-detail',
            templateUrl: './app-details.component.html',
            providers: [app_util_1.AppUtil]
        }),
        __metadata("design:paramtypes", [router_1.Router, router_1.ActivatedRoute, core_1.NgZone,
            cart_service_1.CartService, app_util_1.AppUtil,
            core_1.ChangeDetectorRef, vulcanuxcore_1.RelayService])
    ], AppDetailsComponent);
    return AppDetailsComponent;
}(vulcanuxcore_2.AbstractMicroApplication));
exports.AppDetailsComponent = AppDetailsComponent;
