"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var vulcanuxcore_1 = require("@apporchid/vulcanuxcore");
var vulcanuxcontrols_1 = require("@apporchid/vulcanuxcontrols");
var http_1 = require("@angular/http");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var http_2 = require("@angular/common/http");
var http_3 = require("@angular/common/http");
var routing_1 = require("./routing");
var vulcanuxhighchart_1 = require("@apporchid/vulcanuxhighchart");
var HighchartsService_1 = require("angular2-highcharts/dist/HighchartsService");
var vulcanuxgooglemaps_1 = require("@apporchid/vulcanuxgooglemaps");
function highchartsFactory() {
    var hchart_hstock = require('highcharts/highstock');
    var hchart_export = require('highcharts/modules/exporting');
    var hmap = require('highcharts/highmaps');
    var hchart_more = require('highcharts/highcharts-more');
    var hmap_module = require('highcharts/modules/map');
    var hword_cloud = require('highcharts/modules/wordcloud');
    var hchart_xrange = require('highcharts/modules/xrange');
    var hchart_funnel = require('highcharts/modules/funnel');
    var hchart_export_data = require('highcharts/modules/export-data');
    var hchart_sunburst = require('highcharts/modules/sunburst');
    var hchart_windbarb = require('highcharts/modules/windbarb');
    var hchart_histogram_bellcurve = require('highcharts/modules/histogram-bellcurve');
    var hchart_variable_pie = require('highcharts/modules/variable-pie');
    var hchart_pareto = require('highcharts/modules/pareto');
    var hchart_heatmap = require('highcharts/modules/heatmap');
    var hchart_variwide = require('highcharts/modules/variwide');
    var hchart_sankey = require('highcharts/modules/sankey');
    var hchart_vector = require('highcharts/modules/vector');
    var hchart_parallel_coordinates = require('highcharts/modules/parallel-coordinates');
    hchart_export(hchart_hstock);
    hchart_more(hchart_hstock);
    hchart_export(hmap);
    hchart_export(hchart_hstock);
    hmap_module(hchart_hstock);
    hword_cloud(hchart_hstock);
    hchart_xrange(hchart_hstock);
    hchart_funnel(hchart_hstock);
    hchart_export_data(hchart_hstock);
    hchart_sunburst(hchart_hstock);
    hchart_windbarb(hchart_hstock);
    hchart_histogram_bellcurve(hchart_hstock);
    hchart_variable_pie(hchart_hstock);
    hchart_pareto(hchart_hstock);
    hchart_heatmap(hchart_hstock);
    hchart_variwide(hchart_hstock);
    hchart_sankey(hchart_hstock);
    hchart_vector(hchart_hstock);
    hchart_parallel_coordinates(hchart_hstock);
    return hchart_hstock;
}
exports.highchartsFactory = highchartsFactory;
var SolutionModule = (function () {
    function SolutionModule(injector) {
        this.injector = injector;
        //  WebixComponentsRegistry(entryComponents, injector);
    }
    SolutionModule = __decorate([
        core_1.NgModule({
            imports: [
                routing_1.routing,
                vulcanuxcore_1.VulcanuxCoreModule,
                vulcanuxcontrols_1.VulcanuxControlsModule,
                http_1.HttpModule,
                common_1.CommonModule,
                forms_1.FormsModule,
                http_3.HttpClientModule,
                vulcanuxhighchart_1.VulcanuxHighChartModule,
                vulcanuxgooglemaps_1.VulcanuxGoogleMapsModule
            ],
            providers: [
                http_2.HttpClient,
                {
                    provide: HighchartsService_1.HighchartsStatic,
                    useFactory: highchartsFactory
                }
            ],
        }),
        __metadata("design:paramtypes", [core_1.Injector])
    ], SolutionModule);
    return SolutionModule;
}());
exports.SolutionModule = SolutionModule;
