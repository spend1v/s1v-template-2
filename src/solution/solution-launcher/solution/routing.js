"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = require("@angular/router");
var vulcanuxcontrols_1 = require("@apporchid/vulcanuxcontrols");
var routes = [
    {
        path: '', component: vulcanuxcontrols_1.SolutionLoaderComponent
    }
];
exports.routing = router_1.RouterModule.forChild(routes);
