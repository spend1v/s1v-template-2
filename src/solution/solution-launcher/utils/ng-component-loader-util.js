"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var vulcanuxcore_1 = require("@apporchid/vulcanuxcore");
var NgComponentLoaderUtil = (function () {
    function NgComponentLoaderUtil() {
    }
    NgComponentLoaderUtil.prototype.getAngularComponent = function (componentId, component, config, injector) {
        var _this = this;
        if (injector === void 0) { injector = null; }
        setTimeout(function () {
            var element = document.getElementById(componentId);
            if (element) {
                var loader = new vulcanuxcore_1.NgComponentLoader(injector);
                var compRef = loader.loadComponentAtDom(component, element, function (instance) {
                    var propsMeta = _this.getComponentPropsMetaData(component);
                    _this.setProps(instance, propsMeta, config);
                });
            }
        }, 0);
        return "<div id=" + componentId + " style='height:100%;width:100%;'></div>";
    };
    NgComponentLoaderUtil.prototype.getComponentPropsMetaData = function (component) {
        var props = component.propDecorators;
        var inputParams = [];
        var outputParams = [];
        for (var key in props) {
            if (props.hasOwnProperty(key)) {
                var element = props[key];
                if (element[0].type.prototype.ngMetadataName === 'Input') {
                    inputParams.push(key);
                }
                if (element[0].type.prototype.ngMetadataName === 'Output') {
                    outputParams.push(key);
                }
            }
        }
        return {
            inputParams: inputParams, outputParams: outputParams
        };
    };
    NgComponentLoaderUtil.prototype.setProps = function (instance, propsMeta, config) {
        var inputParams = propsMeta.inputParams;
        var outputParams = propsMeta.outputParams;
        if (inputParams && inputParams.length > 0) {
            inputParams.forEach(function (input) {
                instance[input] = config[input];
            });
        }
        if (outputParams && outputParams.length > 0) {
            outputParams.forEach(function (output) {
                var emitter;
                if (instance[output]) {
                    emitter = instance[output];
                    emitter.subscribe(function (data) {
                        config[output](data);
                    });
                }
            });
        }
    };
    return NgComponentLoaderUtil;
}());
exports.NgComponentLoaderUtil = NgComponentLoaderUtil;
