"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var vulcanuxcontrols_1 = require("@apporchid/vulcanuxcontrols");
var vulcanuxcore_1 = require("@apporchid/vulcanuxcore");
var SessionUtil = (function () {
    function SessionUtil(authService, router, zone) {
        this.authService = authService;
        this.router = router;
        this.zone = zone;
        this.sessionTimeoutWindow = null;
    }
    SessionUtil.prototype.activateSessionTimeout = function () {
        if (!SessionUtil.isSessionActivated) {
            window.addEventListener("mousemove", this.resetTimer.bind(this));
            window.addEventListener("mousedown", this.resetTimer.bind(this));
            window.addEventListener("keypress", this.resetTimer.bind(this));
            window.addEventListener("DOMMouseScroll", this.resetTimer.bind(this));
            window.addEventListener("mousewheel", this.resetTimer.bind(this));
            window.addEventListener("touchmove", this.resetTimer.bind(this));
            window.addEventListener("MSPointerMove", this.resetTimer.bind(this));
            this.startTimer();
            SessionUtil.isSessionActivated = true;
        }
    };
    SessionUtil.prototype.startTimer = function () {
        // let expiryTime = parseInt(localStorage.getItem('session_timeout'));
        if (localStorage.getItem('sessionIdleTime')) {
            var expiryTime = Number(localStorage.getItem('sessionIdleTime'));
            expiryTime = (expiryTime - 120) * 1000;
            // this.timeout = window.setTimeout(this.goInactive.bind(this), expiryTime);
            window['sessionTimeout'] = window.setTimeout(this.goInactive.bind(this), expiryTime);
        }
    };
    SessionUtil.prototype.resetTimer = function (e) {
        if (!$$('winTimeout')) {
            if (window['sessionTimeout']) {
                window.clearTimeout(window['sessionTimeout']);
            }
            this.startTimer();
        }
        else {
            // TODO: remove log
            console.log('not resetting');
        }
    };
    SessionUtil.prototype.goInactive = function () {
        var _this = this;
        console.log('session will be inactive in 2 mins');
        this.closeTimeOutWindow();
        this.sessionExpiryTime = Math.floor((new Date().getTime()) / 1000) + Number(120);
        this.openSessiontimeoutWindow();
        this.interval = setInterval(function () {
            _this.updateClock();
        }, 1000);
    };
    SessionUtil.prototype.openSessiontimeoutWindow = function () {
        var _this = this;
        this.sessionTimeoutWindow = webix.ui({
            view: 'window',
            id: 'winTimeout',
            css: 'vux_confirmation_window',
            modal: true,
            head: {
                view: 'toolbar', paddingY: 10, paddingX: 15, height: 45, elements: [
                    { view: 'icon', icon: 'cogs', css: 'window_header_icon', align: 'right' },
                    { view: 'label', label: 'Session timeout', css: 'vux_h6', }
                ]
            },
            width: 386,
            height: 300,
            position: 'center',
            body: {
                view: 'layout', width: 386, height: 214, paddingX: 45, rows: [
                    { height: 20 },
                    { view: 'label', css: 'vux_p', height: 28, label: 'Your online session will expire in' },
                    { height: 10 },
                    this.getClockTemplate(),
                    { height: 15 },
                    { view: 'label', css: 'vux_p', label: 'Please click "Continue" to keep working;<br/>or click "Log Off" to end your session now. ' },
                    { height: 15 },
                    {
                        cols: [
                            {
                                view: 'button', width: 108, height: 42, value: 'Continue', css: 'continue', click: function () {
                                    _this.handleContinueSession();
                                }
                            },
                            { width: 30 },
                            {
                                view: 'button', width: 108, height: 42, value: 'Log Off', click: function () {
                                    // window.clearTimeout(this.timeout);
                                    _this.handleLogoffSession();
                                }
                            }
                        ]
                    }
                ]
            }
        });
        this.sessionTimeoutWindow.show();
    };
    SessionUtil.prototype.handleLogoffSession = function () {
        var _this = this;
        if (window['sessionTimeout']) {
            window.clearTimeout(window['sessionTimeout']);
        }
        this.closeTimeOutWindow();
        webix.callEvent("onClick", []);
        var appUtil = new vulcanuxcontrols_1.AppUtil();
        appUtil.closeAllApplications();
        this.removeListeners();
        if (this.interval) {
            clearInterval(this.interval);
        }
        vulcanuxcore_1.UsageAnalytics.getInstance().sendLogoutEvent(true);
        this.authService.logout(function () {
            localStorage.setItem('sessionStatus', 'expired');
            _this.zone.run(function () {
                console.log('redirect to login');
                _this.router.navigate([vulcanuxcore_1.StringConstants.lOGIN_PAGE_URL]);
            });
        });
    };
    SessionUtil.prototype.handleContinueSession = function () {
        if (this.interval) {
            clearInterval(this.interval);
        }
        this.closeTimeOutWindow();
        this.resetTimer(null);
    };
    SessionUtil.prototype.closeTimeOutWindow = function () {
        $$('winTimeout') && $$('winTimeout').close();
    };
    SessionUtil.prototype.getTimeRemaining = function () {
        var endTime = this.sessionExpiryTime;
        if (endTime) {
            var t = Number(endTime) - (Math.floor((new Date().getTime()) / 1000));
            var minutes = Math.floor(t / 60);
            var seconds = t % 60;
            return {
                'minutes': minutes,
                'seconds': seconds
            };
        }
    };
    SessionUtil.prototype.getClockTemplate = function () {
        var data = this.getTimeRemaining();
        var clockTemplate = {
            view: "template",
            id: "clockTemplate",
            css: 'time_stamp_container',
            height: 32,
            data: data,
            template: function (obj) {
                return '<span class="number">' + obj.minutes + '</span> <span> min </span> <span class="number">' + obj.seconds + '</span><span> secs</span';
            }
        };
        return clockTemplate;
    };
    SessionUtil.prototype.updateClock = function () {
        var time = this.getTimeRemaining();
        if (time.minutes <= 0 && time.seconds <= 0) {
            this.onSessionExpire();
        }
        else {
            if ($$('clockTemplate')) {
                $$('clockTemplate').setValues(time);
            }
        }
    };
    SessionUtil.prototype.onSessionExpire = function () {
        var _this = this;
        if (this.interval) {
            clearInterval(this.interval);
        }
        this.closeTimeOutWindow();
        console.log('session timed out');
        webix.callEvent("onClick", []);
        var appUtil = new vulcanuxcontrols_1.AppUtil();
        appUtil.closeAllApplications();
        vulcanuxcore_1.UsageAnalytics.getInstance().sendLogoutEvent(true);
        this.authService.logout(function () {
            localStorage.setItem('sessionStatus', 'expired');
            _this.zone.run(function () {
                console.log('redirect to login');
                _this.router.navigate([vulcanuxcore_1.StringConstants.lOGIN_PAGE_URL]);
            });
        });
    };
    SessionUtil.prototype.removeListeners = function () {
        window.removeAllListeners();
    };
    SessionUtil.isSessionActivated = false;
    return SessionUtil;
}());
exports.SessionUtil = SessionUtil;
