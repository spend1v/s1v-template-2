"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function proceesSolutionConfig(config) {
    config.apps.cells[0]['body']['appinfo']['modulePath'] = 'main/apps/datatable/datatable.module.js';
    config.apps.cells[0]['body']['appinfo']['moduleName'] = 'DataTableModule';
    config.apps.cells[0]['body']['appinfo']['rootComponentName'] = 'app-datatable';
    config.apps.cells[1]['body']['appinfo']['modulePath'] = 'main/apps/subscriber/subscriber.module.js';
    config.apps.cells[1]['body']['appinfo']['moduleName'] = 'SubscriberModule';
    config.apps.cells[1]['body']['appinfo']['rootComponentName'] = 'app-subscriber';
    return config;
}
exports.proceesSolutionConfig = proceesSolutionConfig;
