"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var vulcanuxcontrols_1 = require("@apporchid/vulcanuxcontrols");
var router_1 = require("@angular/router");
var vulcanuxcore_1 = require("@apporchid/vulcanuxcore");
var routes = [
    { path: 'login', component: vulcanuxcontrols_1.ProductLogin },
    { path: 'appstore', loadChildren: 'solution/solution-launcher/solutions/appstore/appstore.module#AppstoreModule', canActivate: [vulcanuxcore_1.AuthGuard] },
    { path: '**', loadChildren: 'solution/solution-launcher/solution/solution.module#SolutionModule', canActivate: [vulcanuxcore_1.AuthGuard] },
];
exports.routing = router_1.RouterModule.forRoot(routes, { useHash: true });
